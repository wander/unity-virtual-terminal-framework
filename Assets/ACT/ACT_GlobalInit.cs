﻿using System;
using UnityEngine;
using UnityEngine.UI;
using AdvancedCommandTerminal;

public class ACT_GlobalInit : MonoBehaviour
{
	static ACT_CommandVariable myCoolVariable = ACT_CommandRegister.MakeCommandVariable("foo", "A variable with no special properties.", "hello!", "");
	static ACT_CommandVariable myCoolVariable2 = ACT_CommandRegister.MakeCommandVariable("bar", "A variable stored within the archive file.", "wow!", "");
	static ACT_CommandVariable myCoolVariable3 = ACT_CommandRegister.MakeCommandVariable("soh", "A variable that is a number ONLY.", "15", "-x");
	static ACT_CommandVariable myCoolVariable4 = ACT_CommandRegister.MakeCommandVariable("cah", "A variable that is a number ONLY between 0 and 1", "0", "-x -n 0 -m 1");
	static ACT_CommandVariable myCoolVariable5 = ACT_CommandRegister.MakeCommandVariable("toa", "A variable that is a number ONLY and is archived.", "36.832", "-a -x");

	public static ACT_GUI_WindowManager DefaultWindowManager { get; private set; }
	public static ACT_GlobalInit Singleton { get; private set; }

	private void Start()
	{
		Singleton = this;

		ACT_CommandRegister.MakeCommandFunction("dmesg", ACT_CLI_Programs.DebugReader);
		ACT_CommandRegister.MakeCommandFunction("archive", ACT_CLI_Programs.StoreArchive);

		ACT_CommandRegister.StopAcceptingCommandVariables();
		ACT_ConfigFileHandler.LoadArchive();

		DefaultWindowManager = InstantiateWindowManager();
		InstantiateCommandConsole();
	}

	/*
	 * Create an window management canvas that is a screenspace overlay.
	 */
	private ACT_GUI_WindowManager InstantiateWindowManager()
	{
		GameObject go;
		Canvas canvas;
		ACT_GUI_WindowManager wm;

		go = new GameObject();
		canvas = go.AddComponent<Canvas>();
		canvas.renderMode = RenderMode.ScreenSpaceOverlay;
		wm = go.AddComponent<ACT_GUI_WindowManager>();

		return wm;
	}

	/*
	 * This won't be done from here in the future, but this is fine for now.
	 */
	private void InstantiateCommandConsole()
	{
		GameObject go = (GameObject)Resources.Load("ACT/GUI/CommandConsole");
		Instantiate(go);
	}
}

