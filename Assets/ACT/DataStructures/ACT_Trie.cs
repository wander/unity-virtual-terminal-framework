﻿using System.Collections;
using System.Collections.Generic;

namespace AdvancedCommandTerminal
{
	public class ACT_Trie<T>
	{
		public class TrieNode
		{
			public TrieNode[] children;
			public T data;

			public TrieNode()
			{
				children = new TrieNode[27]; // Each element is a null reference.
			}

			public TrieNode(T data)
			{
				this.data = data;
				children = new TrieNode[27];
			}
		}

		// Supported characters are [a-z] and _, starting from 0 and ending at 26
		private TrieNode rootNode;

		public ACT_Trie ()
		{
			rootNode = new TrieNode();
		}

		public bool Add(string str, T data)
		{
			// Check if the key string is valid
			if (!ValidateString (str))
				return false;

			// Check if the key string already exists
			if (FindNode (str) != null)
				return false;

			int len = str.Length;
			TrieNode currentNode = rootNode;

			for (int i = 0; i < len; i++) {
				int childNum = Char2Num (str [i]);

				if (currentNode.children [childNum] == null) {
					currentNode.children [childNum] = new TrieNode ();
				}

				currentNode = currentNode.children [childNum];
			}

			currentNode.data = data;

			return true;
		}

		public T Search(string str)
		{
			TrieNode node = FindNode (str);

			if (node == null)
				return default(T);
			else
				return node.data;
		}

		private TrieNode FindNode(string str)
		{
			if (!ValidateString (str))
				return null;

			int len = str.Length;
			TrieNode currentNode = rootNode;

			for (int i = 0; i < len; i++) {
				int childNum = Char2Num (str [i]);

				currentNode = currentNode.children [childNum];

				// No child exists, so stop right now.
				if (currentNode == null)
					return null;
			}

			return currentNode;
		}

		private int Char2Num(char c)
		{
			if (c == '_')
				return 26;
			else if (c <= 'z' && c >= 'a')
				return c - 'a';
			else
				return -1;
		}

		private char Num2Char(int n)
		{
			if (n == 26)
				return '_';
			else if (n <= 26 && n >= 0)
				return (char) ((char)n + 'a');
			else
				return '\0';
		}

		/*
		 * Validates a string by finding if numerical representations of its characters can be found.
		 */
		public bool ValidateString(string str)
		{
			int len = str.Length;

			if (len < 1)
				return false;

			for (int i = 0; i < len; i++) {
				if (Char2Num (str [i]) == -1)
					return false;
			}

			return true;
		}
	}
}
