﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AdvancedCommandTerminal
{
	/*
	 * This window management part of the debug system was hacked on from another project, expect
	 * things to not work or look good.
	 */
	public class ACT_GUI_WindowManager : MonoBehaviour 
	{
		private List<ACT_GUI_Window> children = new List<ACT_GUI_Window>(); // List of windows
		private RectTransform 	drawRect; 				// The rect transform all windows exist within.
		private Vector3 		mouseCurrentPosition;
		private Vector3 		mouseLastPosition;
		private Vector3 		mouseDelta;				// Difference between the mouse's current and last position.
		private ACT_GUI_Window 	resizing, moving;		// Windows that we are resizing or moving if any. This is mostly a quick hack.
		private WindowCorners 	resizeCorner = 0;		// The corner we are resizing from if any.

		public ACT_GUI_Window FocussedWindow { get { return children[0]; } }

		public enum WindowCorners
		{
			TOP_LEFT,
			TOP_RIGHT,
			BOTTOM_RIGHT,
			BOTTOM_LEFT
		}

		void Start()
		{
			DontDestroyOnLoad(this);
			drawRect = GetComponent<RectTransform>();
		}

		void Update()
		{
			mouseCurrentPosition = Input.mousePosition;
			mouseDelta = mouseCurrentPosition - mouseLastPosition;

			HandleInput();

			mouseLastPosition = mouseCurrentPosition;
		}

		// Handle input for use with resizing, moving and focusing.
		void HandleInput()
		{
			bool alt = Input.GetKey(KeyCode.LeftControl);
			bool mouseLeftClick = Input.GetKeyDown(KeyCode.Mouse0);
			bool mouseRightClick = Input.GetKeyDown(KeyCode.Mouse1);
			bool mouseLeftHold = (!mouseLeftClick) && Input.GetKey(KeyCode.Mouse0);
			bool mouseRightHold = (!mouseRightClick) && Input.GetKey(KeyCode.Mouse1);

			// Keep moving if we started moving, stop moving if we let go of the mouse button.
			if (moving != null) {
				if (!mouseLeftClick && !mouseLeftHold) {
					// Stop moving the window.
					moving = null;
				} else {
					moving.Position += mouseDelta;
				}
				return;
			}

			// Keep resizing if we started moving, stop resizing if we let go of the mouse button.
			if (resizing != null) {
				if (!mouseRightClick && !mouseRightHold) {
					resizing = null;
				} else {
					switch (resizeCorner) {
						case WindowCorners.TOP_LEFT:
							resizing.ResizeTopLeft(mouseDelta);
							break;
						case WindowCorners.TOP_RIGHT:
							resizing.ResizeTopRight(mouseDelta);
							break;
						case WindowCorners.BOTTOM_RIGHT:
							resizing.ResizeBottomRight(mouseDelta);
							break;
						case WindowCorners.BOTTOM_LEFT:
							resizing.ResizeBottomLeft(mouseDelta);
							break;
					}
				}
				return;
			}

			// Try focus the window under the cursor
			if (mouseLeftClick || mouseRightClick) {
				FocusUnderCursor(); 
			}

			// Start moving the first window under the cursor if one is available.
			if (mouseLeftClick && alt) {
				WindowCorners unused;
				WindowUnderCursor(out moving, out unused);
				return;
			}

			// Start resizing the first window under the cursor if one is available.
			if (mouseRightClick && alt) {
				WindowUnderCursor(out resizing, out resizeCorner);
				return;
			}

		}

		// Draw all windows to a specific RectTransform.
		public void DrawWindowsToRect(RectTransform rect)
		{
			drawRect = rect;
			for (int i = 0; i < children.Count; i++) {
				children[i].GetComponent<RectTransform>().SetParent(drawRect);
			}
		}

		// Add a window to the window manager.
		public void RegisterWindow(ACT_GUI_Window window)
		{
			if (children.Contains(window)) {
				ACT_Debug.LogWarning(window.title + " is already attached to the WM.");
			} else {
				children.Add(window);
			}

			if (drawRect != null) {
				window.GetComponent<RectTransform>().SetParent(drawRect);
				Debug.Log(drawRect);
			} else {
				window.Close();
			}
		}

		// Remove a window from the window manager.
		public void UnregisterWindow(ACT_GUI_Window window)
		{
			if (children.Contains(window)) {
				children.Remove(window);
			} else {
				ACT_Debug.LogError(window.title + " is not registered somehow.");
			}
		}

		// Get the top most window underneath the cursor.
		public bool WindowUnderCursor(out ACT_GUI_Window window, out WindowCorners corner)
		{
			for (int i = 0; i < children.Count; i++) {
				ACT_GUI_Window current = children[i];
				Vector2 halfSize = new Vector2(current.Width, current.Height) / 2.0f;
				Vector2 windowPosition = current.Position;

				// Calculate if the cursor position is inside the window.
				Debug.Log("Mouse: " + mouseCurrentPosition + " Window: " + current.Position);

				bool xFitMax, xFitMin;
				bool yFitMax, yFitMin;

				xFitMax = mouseCurrentPosition.x <= windowPosition.x + halfSize.x;
				xFitMin = mouseCurrentPosition.x >= windowPosition.x - halfSize.x;
				yFitMax = mouseCurrentPosition.y <= windowPosition.y + halfSize.y;
				yFitMin = mouseCurrentPosition.y >= windowPosition.y - halfSize.y;

				if (xFitMax && xFitMin && yFitMax && yFitMin) {
					// Find out which quadrant/corner of the window the mouse cursor is in.
					bool xPlus, yPlus;

					xPlus = mouseCurrentPosition.x >= windowPosition.x;
					yPlus = mouseCurrentPosition.y >= windowPosition.y;

					if (xPlus && yPlus) {
						corner = WindowCorners.TOP_RIGHT;
					} else if (xPlus && !yPlus) {
						corner = WindowCorners.BOTTOM_RIGHT;
					} else if (!xPlus && !yPlus) {
						corner = WindowCorners.BOTTOM_LEFT;
					} else {
						corner = WindowCorners.TOP_LEFT;
					}

					window = current;
					return true;
				}
			}

			corner = 0;
			window = null;
			return false;
		}

		// Bring the top most window underneath the cursor to the front and focus it.
		public void FocusUnderCursor()
		{
			ACT_GUI_Window windowUnder;
			bool foundWindow;
			WindowCorners corner;

			foundWindow = WindowUnderCursor(out windowUnder, out corner);

			if (foundWindow) {
				children.Remove(windowUnder); 
				children.Insert(0, windowUnder);
				Debug.Log(windowUnder.title);
				windowUnder.GetComponent<RectTransform>().SetAsLastSibling();
				windowUnder.hasFocus = true;

				// hack: unfocus everything else.
				for (int i = 0; i < children.Count; i++) {
					if (children[i] != windowUnder) {
						children[i].hasFocus = false;
					}
				}
			}
		}
	}
}
