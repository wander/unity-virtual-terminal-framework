﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace AdvancedCommandTerminal
{
	public class ACT_GUI_CommandTerminal : ACT_GUI_Window
	{
		[SerializeField]
		private Text textOutput;

		[SerializeField]
		private InputField textInput;

		private ACT_TerminalEmulator terminal = new ACT_TerminalEmulator();

		public void Start()
		{
			base.Start();
			terminal.canLock = true;
		}

		public void Update()
		{
			base.Update();
			HandleInput();
			textOutput.text = terminal.BufferToString(); // Not too efficient.
		}

		public void HandleInput() 
		{
			if (!hasFocus)
				return;

			if (Input.GetKey(KeyCode.LeftWindows)) {
				terminal.Unlock(); // Unlocking should cease a program that is running.
				terminal.PrintLn("!!!Break operation!!!");
			}

			if (terminal.Locked) {
				return;
			}

			textInput.Select();

			if (Input.GetKey(KeyCode.Return)) {
				if (textInput.text != "") {
					terminal.Execute(textInput.text);
					textInput.text = "";
				}
			}
		}
	}
}

