﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace AdvancedCommandTerminal
{
	public class ACT_GUI_Window : MonoBehaviour
	{
		public static float MIN_WINDOW_SIZE = 64;

		public string title;

		private RectTransform window;
		public bool hasFocus = false;
		public float Width { get { return window.rect.width; }}
		public float Height { get { return window.rect.height; }}
		public Vector3 Position { get { return window.position; } 
			set { window.position = value; } }

		public void Start () 
		{
			window = GetComponent<RectTransform>();

			if (window == null)
			{
				ACT_Debug.LogFatal("ACT: Window Manager script attached to an object with no base RectTransform. This is required for resizing and moving.");
			}

			ACT_GlobalInit.DefaultWindowManager.RegisterWindow(this);
		}

		// Update is called once per frame
		public void Update () 
		{		

		}

		public void ResizeTopRight(Vector2 amount)
		{
			Vector2 sizeDelta = ClampSizeDelta(amount);
			Vector3 positionDelta = sizeDelta / 2;

			window.sizeDelta += sizeDelta;
			window.position += positionDelta;
		}

		public void ResizeTopLeft(Vector2 amount)
		{
			amount.x *= -1;

			Vector2 sizeDelta = ClampSizeDelta(amount);
			Vector3 positionDelta = sizeDelta / 2;

			positionDelta.x *= -1;

			window.sizeDelta += sizeDelta;
			window.position += positionDelta;
		}

		public void ResizeBottomRight(Vector2 amount)
		{
			amount.y *= -1;

			Vector2 sizeDelta = ClampSizeDelta(amount);
			Vector3 positionDelta = sizeDelta / 2;

			positionDelta.y *= -1;

			window.sizeDelta += sizeDelta;
			window.position += positionDelta;
		}

		public void ResizeBottomLeft(Vector2 amount)
		{
			amount.y *= -1;
			amount.x *= -1;

			Vector2 sizeDelta = ClampSizeDelta(amount);
			Vector3 positionDelta = sizeDelta / 2;

			positionDelta.x *= -1;
			positionDelta.y *= -1;

			window.sizeDelta += sizeDelta;
			window.position += positionDelta;
		}

		Vector2 ClampSizeDelta(Vector2 sizeDelta)
		{
			Vector2 desiredSize;
			Vector2 newDelta;

			desiredSize = window.rect.size + sizeDelta;
			newDelta = sizeDelta;

			if (desiredSize.x < MIN_WINDOW_SIZE) {
				newDelta.x += MIN_WINDOW_SIZE - desiredSize.x;
			}

			if (desiredSize.y < MIN_WINDOW_SIZE) {
				newDelta.y += MIN_WINDOW_SIZE - desiredSize.y;
			}

			return newDelta;
		}

	/*
	void HandleResize()
	{
		if (!Input.GetKey(KeyCode.LeftAlt))
		{
			return;
		}

		Vector3 mouseOffset = Input.mousePosition - window.position;

		// Offsetting enum to work out the corner. Similar idiom to adding moves to find the winner in paper scissors rock.

		Vector3 newDelta = mouseDelta;

		Corner corner = 0;

		int xsign = 1, ysign = 1;

		if (mouseOffset.x <= 0) 
		{
			newDelta.x *= -1.0f;
			xsign = -1;
			corner += 2;
		}			

		if (mouseOffset.y <= 0)
		{
			ysign = -1;
			newDelta.y *= -1.0f;
			corner += 1;
		}
			

		// We now want to pull the desired corner out by our delta.

		// Push the object backwards by half

		Vector2 sizeDelta = new Vector2(newDelta.x, newDelta.y);
		Vector3 positionDelta = mouseDelta / 2;
		Vector2 newSize = window.rect.size + sizeDelta;

		if (newSize.x < MIN_WINDOW_SIZE)
		{
			float change = MIN_WINDOW_SIZE - newSize.x;
			sizeDelta.x += change;
			positionDelta.x += (xsign * change) / 2.0f;
			Debug.Log(change);
		}

		if (newSize.y < MIN_WINDOW_SIZE)
		{
			float change = MIN_WINDOW_SIZE - newSize.y;
			sizeDelta.y += change;
			positionDelta.y += (ysign * change) / 2.0f;
		}

		window.sizeDelta += sizeDelta;
		window.position = window.position + positionDelta;
	}
	*/

		public void Minimize()
		{

		}

		public void Close()
		{

		}
	}
}

