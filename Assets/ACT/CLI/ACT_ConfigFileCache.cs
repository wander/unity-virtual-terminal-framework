﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;

namespace AdvancedCommandTerminal
{
	public static class ACT_ConfigFileHandler
	{
		private const string CFG_PATH = "ACT_Config/";
		private const string AUTOEXEC_FILE = "autoexec.cfg";
		private const string ARCHIVE_FILE = "archive.cfg";

		private static Dictionary<string, string> archive = new Dictionary<string, string>();
		private static Dictionary<string, bool> writableFiles = new Dictionary<string, bool>();
		private static bool startedArchiving = false;

		public static bool ExecuteConfig(string file)
		{
			try {
				StreamReader sr = new StreamReader(CFG_PATH + file);
				while (!sr.EndOfStream) {
					string command = sr.ReadLine();
					ACT_StaticTerminal.Execute(command);
				}
				return true;
			} catch (Exception e) {
				ACT_Debug.LogError("Unable to open file \'" + file + "\', Error: " + e.Message);
				return false;
			}
		}
			
		/*
		 * Saves all archived information to the disk.
		 * Makes sure that the file is completely written to before overwriting the old
		 * archive file. Since this operating might spawn an hour glass this means that
		 * there is no chance that the program will crash while writing and run everything.
		 */
		public static void StoreArchive() 
		{
			StreamWriter file = new StreamWriter(CFG_PATH + ARCHIVE_FILE + ".new");
			foreach (KeyValuePair<string, string> cmd in archive) {
				file.WriteLine(cmd.Key + " " + cmd.Value);
				ACT_Debug.LogNormal("Writing to file: " + cmd.Key + " " + cmd.Value);
			}
			file.Close();

			File.Copy(CFG_PATH + ARCHIVE_FILE + ".new", CFG_PATH + ARCHIVE_FILE, true);
			File.Delete(CFG_PATH + ARCHIVE_FILE + ".new");
		}

		/*
		 * Loads the archive file, runs every command in it and then makes sure that every
		 * command inside it is cached so that variables can be modified and saved again.
		 */
		public static void LoadArchive()
		{
			try {
				StreamReader sr = new StreamReader(CFG_PATH + ARCHIVE_FILE);
				archive.Clear(); // Easier than syncing the old archive with the new dictionary.
				while (!sr.EndOfStream) {
					StringBuilder commandStr = new StringBuilder();
					StringBuilder argsStr = new StringBuilder();

					while ((char) sr.Peek() != ' ') {
						commandStr.Append((char) sr.Read());
					}

					while ((char) sr.Peek() != '\n' && (char) sr.Peek() != ';') {
						argsStr.Append((char) sr.Read());
					}

					sr.Read(); // Skip the \n or ;

					if (ACT_CommandRegister.GetCommand(commandStr.ToString()).Archive) {
						archive.Add(commandStr.ToString(), argsStr.ToString());
						ACT_Debug.LogNormal("Caching: " + commandStr.ToString() + " " + argsStr.ToString());
						ACT_StaticTerminal.Execute(commandStr.ToString() + " " + argsStr.ToString());
					} else {
						ACT_Debug.LogWarning("Found malformed command in archive, removing.");
					}
				}
				sr.Close();
			} catch (Exception e) {
				// Do nothing.
			}

			// Do nothing actually, the only case we need to open an archive is if it has stuff in it.
			startedArchiving = true;
		}

		public static void ArchiveIfAvailable(string command, string arg)
		{
			if (!startedArchiving) {
				return;
			}

			if (archive.ContainsKey(command)) {
				archive[command] = arg;
			} else {
				archive.Add(command, arg);
			}
		}
	}
}

