﻿using System;

namespace AdvancedCommandTerminal
{
	public interface ACT_ICommandBase
	{
		bool Archive { get; }
		void Execute(string[] args, ACT_TerminalEmulator output);
	}
}

