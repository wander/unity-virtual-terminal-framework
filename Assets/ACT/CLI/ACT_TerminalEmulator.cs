﻿using System;
using System.Text;
using UnityEngine;

namespace AdvancedCommandTerminal
{
	public class ACT_TerminalEmulator
	{
		private const int 		MAX_SCROLLBACK 	= 200;					// The maximum number of lines stored in the output.
		private StringBuilder 	textBuffer 		= new StringBuilder();	// The default text output.
		private StringBuilder 	tempBuffef		= new StringBuilder();	// An additional output in case we want a program to do some ncurses style stuff where the buffer is restored.
		private int 			numLines 		= 0;					// Line count.
		public  bool			canLock			= false;

		public bool Locked { get; private set; }

		public ACT_TerminalEmulator()
		{
			
		}

		public void Lock()
		{
			Locked = true;
		}

		public void Unlock()
		{
			Locked = false;
		}

		public void Execute(string command)
		{
			// Execute with this terminal emulator as the output.
			PrintLn("> " + command);

			string[] cmdv = command.Split (';');
			int cmdc = cmdv.Length;

			for (int i = 0; i < cmdc; i++) 
			{
				string[] 			argv;
				ACT_ICommandBase 	cmdp;

				argv = Parametrise(cmdv[i]);

				cmdp = ACT_CommandRegister.GetCommand(argv[0]);

				if (cmdp == null) {
					// Command not found.
					PrintLn("Command " + argv[0] + " not found.");
					return;
				}

				cmdp.Execute(argv, this);
			}
		}

		public static string[] Parametrise(string cmd) 
		{
			// Please replace this implementation, it is very poor.
			cmd = cmd.Trim(); // Remove the beggining and ending whitespace.

			int 			strlen			= cmd.Length;
			bool 			inquotes 		= false;
			int 			argc 			= 0;
			string[] 		argvbuffer 		= new string[strlen];
			StringBuilder 	currentparam 	= new StringBuilder();
			bool 			skipspace 		= true;

			for (int i = 0; i < strlen; i++) 
			{
				if (cmd[i] == '\"') 
				{
					if (inquotes) 
					{
						argvbuffer[argc] = currentparam.ToString();
						++argc;
						currentparam = new StringBuilder();
					} 

					inquotes = !inquotes;
					continue;
				}

				if (inquotes) 
				{
					currentparam.Append(cmd[i]);
					continue; // We added this character so I don't know what else we could possibly do.
				}

				if (cmd[i] == ' ') 
				{
					if (skipspace) 
					{
						skipspace = false;
						argvbuffer[argc] = currentparam.ToString();
						++argc;
						currentparam = new StringBuilder();
						continue;
					} 
					else 
					{
						//currentparam.Append(' '); // cmd[i]
						continue;
					}
				}

				skipspace = true;
				currentparam.Append(cmd[i]);
			}

			if (currentparam.ToString() != string.Empty) 
			{
				argvbuffer[argc] = currentparam.ToString();
				argc++;
			}

			string[] output = new string[argc]; // Resize the array because we are dumb.
			for (int i = 0; i < argc; i++) 
			{
				output[i] = argvbuffer[i];
			}

			return output;
		}

		/*
		 * Simply adds a string to the text buffer.
		 * Additionally checks for newlines and increments the number of lines.
		 */
		public void Print(string str)
		{
			int strLen = str.Length;

			for (int i = 0; i < strLen; i++) {
				if (str[i] == '\n') {
					++numLines;
				}
				textBuffer.Append(str[i]);
			}

			LimitScrollback();
		}

		/*
		 * Adds a string to the text buffer and adds a new line afterwards.
		 */ 
		public void PrintLn(string str)
		{
			Print(str + "\n");
			Debug.Log(str);
		}

		/*
		 * Clears the text buffer.
		 */
		public void Clear()
		{
			textBuffer = new StringBuilder();
		}

		/*
		 * Gets the output of the text buffer, or basically what the terminal should have inside it.
		 */
		public string BufferToString()
		{
			return textBuffer.ToString();
		}

		/*
		 * Attempts to remove the first line of the text buffer if the scrollback limit is reached.
		 */
		private void LimitScrollback()
		{
			if (numLines > MAX_SCROLLBACK) {
				RemoveScrollbackLine();
			}
		}

		/*
		 * Removes the first line from the text buffer, used for limiting line scrollback.
		 */
		private void RemoveScrollbackLine()
		{
			int buffLen = textBuffer.Length;
			bool lineFound = false; // Used in case someone sets the maximum scrollback to a negative number or something silly.

			int i;
			for (i = 0; i < buffLen; i++) {
				if (textBuffer[i] == '\n') {
					lineFound = true;
					break; // stop the look here because i is our end index.
				}
			}

			if (lineFound == true) {
				textBuffer.Remove(0, i);
			}
		}
	}
}

