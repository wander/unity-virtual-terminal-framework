﻿using System;

namespace AdvancedCommandTerminal
{
	public static class ACT_CommandRegister
	{
		private static ACT_Trie<ACT_ICommandBase> commands = new ACT_Trie<ACT_ICommandBase>();
		private static bool acceptCommandVariables = true;

		public static void MakeCommandFunction(string command, ACT_CommandFunction.Signature functionDelegate)
		{
			if (!acceptCommandVariables) {
				ACT_Debug.LogError("ACT: Tried to create the command \'" + command + "\', but the engine is not allowed to create command past the first frame.");
				return;
			}

			if (commands.Search (command) != null) {
				ACT_Debug.LogError("ACT: Tried to create the command \'" + command + "\', but it already exists.");
				return;
			}

			if (!commands.ValidateString (command)) {
				ACT_Debug.LogError("ACT: Tried to create the command \'" + command + "\', but it contains invalid characters. a-Z and _ only.");
				return;
			}

			ACT_CommandFunction wrapper = new ACT_CommandFunction(functionDelegate);
			commands.Add(command, wrapper);

			ACT_Debug.LogNormal("ACT: Command \'" + command + "\' successfully registered.");
		}

		public static ACT_CommandVariable MakeCommandVariable(string command, string description, string value, string flags)
		{
			if (!acceptCommandVariables) {
				ACT_Debug.LogError("ACT: Tried to create the command \'" + command + "\', but the engine is not allowed to create command past the first frame.");
				return null;
			}

			if (commands.Search (command) != null) {
				ACT_Debug.LogError("ACT: Tried to create the command \'" + command + "\', but it already exists.");
				return null;
			}

			if (!commands.ValidateString (command)) {
				ACT_Debug.LogError("ACT: Tried to create the command \'" + command + "\', but it contains invalid characters. a-Z and _ only.");
				return null;
			}

			ACT_CommandVariable variable;

			if (flags == "") { // Dumb hack
				variable = new ACT_CommandVariable(description, value);
			} else {
				variable = new ACT_CommandVariable(description, value, flags);
			}

			commands.Add(command, variable);

			ACT_Debug.LogNormal("ACT: Command \'" + command + "\' successfully registered.");

			return variable;
		}

		public static ACT_ICommandBase GetCommand(string command)
		{
			ACT_ICommandBase cmd;
			cmd = commands.Search(command);
			return cmd;
		}

		/*
		 * CVars are static allocation only. This is to ensure all commands are registered
		 * before configuration files are read. There may be a case where you need a specific
		 * object to have a cvar. In this case, please use a command function that sets the
		 * required variable.
		 */
		public static void StopAcceptingCommandVariables()
		{
			acceptCommandVariables = false;
		}
	}
}

