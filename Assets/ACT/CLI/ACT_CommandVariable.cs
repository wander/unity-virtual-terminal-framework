﻿using System;
using UnityEngine;
using AdvancedCommandTerminal;
using System.Text;

namespace AdvancedCommandTerminal
{
	/*
	 * Sometimes it is easier to pray than to worry about type-safety. This mindset
	 * is the basis of all C style strongly typed languages. It is because of this
	 * that finding an implementation of CVars in a similar way to this one is a
	 * isn't too much cause for alarm. Imagine if this was in C++ and used a fancy
	 * template class for variables that require it to exist as different types.
	 * Now that's scary.
	 */
	public class ACT_CommandVariable : ACT_ICommandBase
	{
		public string 	StringValue { get; private set; }
		public int		IntValue 	{ get; private set; }
		public float	FloatValue 	{ get; private set; }
		public bool		Archive		{ get; private set; }

		private double 	min;
		private double 	max;
		private bool 	hasMin;
		private bool 	hasMax;
		private bool	numberOnly;
		private string 	description;

		public ACT_CommandVariable(string description, string value)
		{
			Archive = false;
			numberOnly = false;
			this.description = description;
			SetAll(value);
		}

		public ACT_CommandVariable(string description, string value, string flags) : this(description, value)
		{
			string[] args = flags.Split(new char[]{' '});
			int argc = args.Length;
			bool valid = true;

			for (int i = 0; i < argc; i++) {
				if (!valid) {
					break;
				}

				switch (args[i]) {
					case "-n":
						++i; // Skip next because next is the argument for min.						
						valid = double.TryParse(args[i], out min);
						hasMin = true;
						break;

					case "-m":
						++i; // Skip next because next is the argument for max.
						valid = double.TryParse(args[i], out max);
						hasMax = true;
						break;

					case "-a":
						Archive = true;
						break;

					case "-x":
						valid = IsNumber(value);
						numberOnly = true;
						break;

					default:
						valid = false;
						break;
				}
			}

			if (!valid) {
				ACT_Debug.LogFatal("ACT: Fatal error, tried to create a command with invalid arguments. The command has the description: " + description);
			}
		}

		/*
		 * If str isn't a valid number then IntValue and FloatValue will be set
		 * to some random garbage. Hopefully in that case you require it to be
		 * a string and nothing else.
		 */
		private void SetAll(string str)
		{
			StringValue = str;
			double num;

			double.TryParse(str, out num);

			if (hasMin) {
				if (num < min) {
					num = min;
				}
			}

			if (hasMax) {
				if (num > max) {
					num = max;
				}
			}

			FloatValue = (float) num;
			IntValue = (int) num;
		}

		public void Execute(string[] args, ACT_TerminalEmulator output)
		{
			if (args.Length < 2) {
				output.PrintLn(GetInfo());
				return;
			}

			if (args.Length > 3) {
				output.PrintLn(args[0] + " is a CVar and CVars do not take more than one argument. Use -h or no arguments for more info.");
				return;
			}

			if (args[1].Equals("-h")) {
				output.PrintLn(GetInfo());
				return;
			}

			Debug.Log(numberOnly);

			if (numberOnly && !IsNumber(args[1])) {
				output.PrintLn(args[0] + " has be to a valid number. It cannot be a normal string.");
				return;
			}

			// Set the variable here.
			SetAll(args[1]);

			output.PrintLn(args[0] + " <= " + args[1]);

			if (Archive) {
				output.PrintLn(args[0] + " changed in the archive. Type archive to write changes to disk.");
				ACT_ConfigFileHandler.ArchiveIfAvailable(args[0], args[1]);
			}
		}

		private string GetInfo()
		{
			StringBuilder str = new StringBuilder();
			str.Append("Value: ");
			str.AppendLine(StringValue);
			str.Append("Description: ");
			str.AppendLine(description);

			if (hasMin) {
				str.Append("Min: ");
				str.AppendLine(min.ToString()); // LAZY: Please make this for ints too!
			}

			if (hasMax) {
				str.Append("Max:");
				str.AppendLine(max.ToString());
			}

			return str.ToString();
		}

		private bool IsNumber(string str)
		{
			bool valid = false;
			double testVal = 0.0;

			valid = double.TryParse(str, out testVal);

			if (valid)
				Debug.Log("double");
			else
				Debug.Log("Not");

			return valid;
		}
	}
}

