﻿using System;

namespace AdvancedCommandTerminal
{
	public class ACT_CommandFunction : ACT_ICommandBase
	{
		public delegate void Signature(string[] args, ACT_TerminalEmulator output);
		public Signature functionDelegate;
		public bool Archive { get; private set; }

		public ACT_CommandFunction(Signature functionDelegate)
		{
			this.functionDelegate = functionDelegate;
		}

		public void Execute(string[] args, ACT_TerminalEmulator output)
		{
			if (functionDelegate != null) {
				functionDelegate (args, output);
			} else {
				ACT_Debug.LogError("ACT: The function bound to " + args [0] + " is a null reference. Please make sure your command functions don't get garbage collected.");
			}
		}
	}
}

