﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AdvancedCommandTerminal;

public static class ACT_CLI_Programs
{
	public static void DebugReader(string[] args, ACT_TerminalEmulator output)
	{
		int argc = args.Length;

		if (argc > 1)
			return;

		MonoBehaviour shed = (MonoBehaviour)ACT_GlobalInit.Singleton;
		output.Lock();
		shed.StartCoroutine(DebugReaderCoroutine(1.0f/5.0f, output));
	}

	private static IEnumerator DebugReaderCoroutine(float interval, ACT_TerminalEmulator output)
	{
		while (output.Locked) {
			output.PrintLn("This program is controlling the terminal!");
			yield return new WaitForSeconds(interval);
		}
	}

	public static void StoreArchive(string[] args, ACT_TerminalEmulator output) 
	{
		if (args.Length > 1) {
			output.PrintLn(args[0] + " does not take any arguments.");
		} else {
			output.PrintLn("Writing marked CVar changes to the archive file.");
			ACT_ConfigFileHandler.StoreArchive();
		}
	}
}
