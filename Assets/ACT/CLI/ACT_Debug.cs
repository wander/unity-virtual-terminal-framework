﻿using System;
using System.Text;
using AdvancedCommandTerminal;
using UnityEngine;

/*
 * This class contains everything needed by the command 'dmesg'
 * The point of dmesg is to provide a unified output with different channels.
 */
public static class ACT_Debug
{
	private static ACT_Trie<StringBuilder> debugChannels = new ACT_Trie<StringBuilder>();

	public static void LogNormal(string msg)
	{
		Debug.Log(msg);
	}

	public static void LogWarning(string msg)
	{
		Debug.LogWarning(msg);
	}

	public static void LogError(string msg)
	{
		Debug.LogError(msg);
	}

	public static void LogFatal(string msg)
	{
		Debug.LogError("FATAL: " + msg);
	}

	public static void LogTerminate(string msg)
	{
		Debug.LogError("!!!UNRECOVERABLE ERROR!!! " + msg);
	}

	public static void LogToChannel(string channel, string msg)
	{

	}
}

