﻿using System;

namespace AdvancedCommandTerminal
{
	public static class ACT_StaticTerminal
	{
		static ACT_TerminalEmulator terminal = new ACT_TerminalEmulator();

		public static void Execute(string cmd)
		{
			ACT_Debug.LogNormal("Executing on the static terminal: " + cmd);
			terminal.Execute(cmd);
		}	
	}
}
