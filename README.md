Here's what this does.

<ul>
<li>Implements a command console system.</li>
<li>Allows said command console system to interact with CVars and CFunctions.</li>
<li>Allows CFunctions to take over the command console and run over multiple frames.</li>
<li>Allows creation of multiple command consoles for use with these functions. One might have a console for input and a console to use a debug log tailing program.</li>
<li>Allows reading of configuration files. These config files are just long lists of commands. These are read and executed by a command console that the user can't see and doesn't need to.</li>
<li>Allows CVar and CFunction archival. Obviously being able to archive and execute programs that run over multiple frames would be bad (especially if they ran infinitely) so it doesn't do that. Archival is the process of having certain command modifications being stored and executed every time the game runs.</li>
<li>Allows binding of CFunctions for input. Think "bind w +walk" and then that being saved inside an archive file.</li>
</ul>

Basically it's a command line that can replace or improve on Unity's built-in
features. Things you might find useful are input rebinding at runtime, storing
user settings in a config file, balancing your game without exiting it and more
in-depth debugging.

More information forth-coming.